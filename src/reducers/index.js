import { combineReducers } from 'redux';
import productos_reducer from './productos_reducers';

export default combineReducers({
    productos: productos_reducer
});