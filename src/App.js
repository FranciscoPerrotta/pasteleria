import React, { Component, Fragment } from 'react'; 

//  Components
import Header from './components/Header';
import Productos from './components/Products';
import NuevoProducto from './components/NuevoProducto'; 
import EditarProducto from './components/EditarProducto'; 

//  Routing
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// Redux
import { Provider } from 'react-redux';
import store from './store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Fragment>
            <Header />
              <div className="container">
                <Switch>
                  <Route exact path="/" component={Productos} />
                  <Route exact path="/productos/nuevo" component={NuevoProducto} />
                  <Route exact path="/productos/editar/:id" component={EditarProducto} />
                </Switch> 
              </div>
          </Fragment>
        </Router> 
      </Provider>
    );
  }
}

export default App;
