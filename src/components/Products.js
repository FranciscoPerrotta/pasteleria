import React, { Component } from 'react';

//  REDUX
import { connect } from 'react-redux';
import { mostrarProductos, borrarProducto } from '../actions/productos_actions';

//  Components
import Producto from './Product';

class Productos extends Component {
 
    componentDidMount() {
        this.props.mostrarProductos();
    }

    render() { 
        const {productos} = this.props; 
        return ( 
            <React.Fragment>
                <h2 className="text-center my-5">Productos</h2>
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <ul>
                            {productos.map(producto => (
                                <Producto 
                                    key={producto.id}
                                    info={producto}
                                    borrarProducto={this.props.borrarProducto}
                                />
                            ))}    
                        </ul>
                    </div>
                </div>
            </React.Fragment>
        );
    }

}

const mapStateToProps = state => ({
    productos: state.productos.productos
})

export default connect(mapStateToProps, { mostrarProductos, borrarProducto })(Productos);